import pygame
import random
import time
import requests

from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    K_SPACE,
    KEYDOWN,
    K_RETURN,
    K_BACKSPACE,
    QUIT,
    FULLSCREEN,
    DOUBLEBUF,
)

# colors
white = (255,255,255)
black = (0,0,0)
bright_red = (255,0,0)
bright_green = (0,255,0)
red = (200,0,0)
green = (0,200,0)
light_green = (43, 181, 80)

# screen size
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# game user and score
user = ''
score = 0

# enemy speed decide based on the game time
# when time increased, enemy speed will increase
last_time = time.time()
enemy_speed = 3000

# game status
home = True
name = False
running = False
end = False

#pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.mixer.init()
pygame.init()
clock = pygame.time.Clock()

# set double buffering and bits per pixel
flags = FULLSCREEN | DOUBLEBUF
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), flags)

# start image
#str_img = pygame.image.load('media/start-bg.png').convert()
str_img = pygame.image.load('media/freedomfi.png').convert()
str_img = pygame.transform.scale(str_img,(SCREEN_WIDTH,SCREEN_HEIGHT))

# name image
name_img = pygame.image.load('media/name-img.png').convert()
name_img = pygame.transform.scale(name_img,(SCREEN_WIDTH,SCREEN_HEIGHT))

# background images
run_img = pygame.image.load('media/run-bg.png').convert()
run_img = pygame.transform.scale(run_img,(SCREEN_WIDTH,SCREEN_HEIGHT))
background_rect = run_img.get_rect()

# game end image
end_img = pygame.image.load('media/game-end.png').convert()
end_img = pygame.transform.scale(end_img,(SCREEN_WIDTH,SCREEN_HEIGHT))

# player
player_img = pygame.image.load("media/freedomfi-rocket.png").convert_alpha()

# differnet points
points = ['media/point1.png', 'media/point2.png', 'media/point3.png', 'media/point4.png', 'media/point5.png']
point_images = []
for image in points:
    point_images.append(pygame.image.load(image).convert_alpha())

# differnt enemies
enimies = ['media/enime1.png', 'media/enime2.png', 'media/enime3.png']
enemy_images = []
for image in enimies:
    enemy_images.append(pygame.image.load(image).convert_alpha())

# differnt bullets
bullet_img = pygame.image.load('media/missile.png').convert()

# background sound
pygame.mixer.music.load("media/Apoxode_-_Electric_1.mp3")
pygame.mixer.music.play(loops=-1)

# action sounds
move_up_sound = pygame.mixer.Sound("media/Rising_putter.ogg")
move_down_sound = pygame.mixer.Sound("media/Falling_putter.ogg")
collision_sound = pygame.mixer.Sound("media/Collision.ogg")
shooting_sound = pygame.mixer.Sound('media/pew.wav')
explore_sound = pygame.mixer.Sound('media/explosion.wav')

# fonts
small_text = pygame.font.Font("freesansbold.ttf", 20)
large_text = pygame.font.Font('freesansbold.ttf', 80)

# add enemy event
ADDENEMY = pygame.USEREVENT + 1
#pygame.time.set_timer(ADDENEMY, random.randint(250, 750))
pygame.time.set_timer(ADDENEMY, enemy_speed)

# add cloud event
ADDCLOUD = pygame.USEREVENT + 2
pygame.time.set_timer(ADDCLOUD, random.randint(1000, 2000))

# enimies and coulds
enemies = pygame.sprite.Group()
clouds = pygame.sprite.Group()
bullets = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()


class InputBox:
    def __init__(self, x, y, w, h, text=''):
        self.rect = pygame.Rect(x, y, w, h)
        self.color = black
        self.text = text
        self.txt_surface = small_text.render(text, True, self.color)

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                global user
                user = self.text
                if user != '':
                    self.text = ''
                    run_game()
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            else:
                self.text += event.unicode
            # re-render the text
            self.txt_surface = small_text.render(self.text, True, self.color)

    def draw(self, screen):
        screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))
        pygame.draw.rect(screen, self.color, self.rect, 2)


'''
class Player(pygame.sprite.Sprite):
    def load_image(self, image):
        curr_image = pygame.image.load(image)
        curr_image.set_colorkey((0, 0, 0), RLEACCEL)
        curr_image = pygame.transform.scale(curr_image, (100, 100))
        return curr_image

    def __init__(self):
        super(Player, self).__init__()
        self.images_running = []
        self.images_jumping = []

        self.ir = 0
        while self.ir < 10:
            self.ir_num = "media/player/Run__00" + str(self.ir) + ".png"
            self.images_running.append(self.load_image(self.ir_num))
            self.ir = self.ir + 1

        self.ij = 0
        while self.ij < 10:
            self.ij_num = "media/player/Jump__00" + str(self.ij) + ".png"
            self.images_jumping.append(self.load_image(self.ij_num))
            self.ij = self.ij + 1

        self.index_running = 0
        self.image_running = self.images_running[self.index_running]

        self.index_jumping = 0
        self.image_jumping = self.images_jumping[self.index_jumping]

        self.image = self.image_running
        self.rect = pygame.Rect(5, 350, 100, 100)

        self.shoot_delay = 300
        self.last_shot = pygame.time.get_ticks()

    def update(self, pressed_keys):
        if pressed_keys[K_UP]:
            self.index_jumping = self.index_jumping + 1
            if self.index_jumping >= len(self.images_jumping):
                self.index_jumping = 0
            self.image_jumping = self.images_jumping[self.index_jumping]
            self.image = self.image_jumping
            self.rect.move_ip(0, -10)
            move_up_sound.play()
        if pressed_keys[K_DOWN]:
            self.index_jumping = self.index_jumping + 1
            if self.index_jumping >= len(self.images_jumping):
                self.index_jumping = 0
            self.image_jumping = self.images_jumping[self.index_jumping]
            self.image = self.image_jumping
            self.rect.move_ip(0, 10)
            move_down_sound.play()
        if pressed_keys[K_LEFT]:
            self.index_running = self.index_running + 1
            if self.index_running >= len(self.images_running):
                self.index_running = 0
            self.image_running = self.images_running[self.index_running]
            self.image = self.image_running
            self.rect.move_ip(-10, 0)
        if pressed_keys[K_RIGHT]:
            self.index_running = self.index_running + 1
            if self.index_running >= len(self.images_running):
                self.index_running = 0
            self.image_running = self.images_running[self.index_running]
            self.image = self.image_running
            self.rect.move_ip(10, 0)
        if pressed_keys[K_SPACE]:
            self.image_running = self.images_running[1]
            self.image = self.image_running
            self.shoot()

        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        elif self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            bullet = Bullet(self.rect.centerx - 5, self.rect.top + 75)
            all_sprites.add(bullet)
            bullets.add(bullet)
            shooting_sound.play()


'''
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.image = player_img
        self.image.set_colorkey((255, 255, 255), RLEACCEL)
        self.shoot_delay = 300
        self.last_shot = pygame.time.get_ticks()
        self.rect = self.image.get_rect(
            center=(
                50,
                SCREEN_HEIGHT/2 - 10,
           )
        )

    def update(self, pressed_keys):
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -10)
            move_up_sound.play()
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 10)
            move_down_sound.play()
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-10, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(10, 0)
        if pressed_keys[K_SPACE]:
            self.shoot()

        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        elif self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            bullet = Bullet(self.rect.centerx - 35, self.rect.top + 35)
            all_sprites.add(bullet)
            bullets.add(bullet)
            shooting_sound.play()


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = bullet_img
        self.image.set_colorkey(black)
        self.rect = self.image.get_rect()
        self.rect.bottom = y
        self.rect.centerx = x
        self.speedy = -15

    def update(self):
        self.rect.move_ip(10, 0)
        if self.rect.left > SCREEN_WIDTH:
            self.kill()


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()
        self.image = random.choice(enemy_images)
        self.image.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.image.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
           )
        )
        #self.speed = random.randint(5, 20)
        self.speed = 5

    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()


class Cloud(pygame.sprite.Sprite):
    def __init__(self):
        super(Cloud, self).__init__()
        self.image = random.choice(point_images)
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speed = random.randint(1, 5)

    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()


player = Player()
all_sprites.add(player)
name_box = InputBox(300, 300, 200, 32)


# start game
def start_game():
    # reset game variable
    global home
    home = True

    global name
    name = False

    global running
    running = False

    global end
    end = False

    global user
    user = ''

    global score
    score = 0

    global last_time
    last_time = time.time()

    global enemy_speed
    enemy_speed = 3000
    pygame.time.set_timer(ADDENEMY, enemy_speed)

    # reset game objects
    global player
    player = Player()

    global enemies
    enemies.empty()
    #enemies = pygame.sprite.Group()

    global clouds
    clouds.empty()
    #clouds = pygame.sprite.Group()

    global all_sprites
    all_sprites.empty()
    #all_sprites = pygame.sprite.Group()
    all_sprites.add(player)

    pygame.mixer.music.play(loops=-1)


# name game
def name_game():
    global home
    home = False

    global name
    name = True

    global running
    running = False

    global end
    end = False


# run game
def run_game():
    global home
    home = False

    global name
    name = False

    global running
    running = True

    global end
    end = False

    pygame.mixer.music.play(loops=-1)


# end game
def end_game():
    global home
    home = False

    global name
    name = False

    global running
    running = False

    global end
    end = True

    publish_score()

    pygame.mixer.music.stop()


# publish score to rahasak
def publish_score():
    #api = "http://localhost:8761/api/scores"
    api = "http://52.188.160.238:8761/api/scores"

    json = {
        "id": str(int(time.time())),
        "execer": "admin:admin",
        "messageType": "addScore",
        "digsig": "",
        "userId": user,
        "score": score,
        "denominator": 0
    }
    headers = {'Content-Type': 'application/json'}

    try:
        r = requests.post(api, json=json, headers=headers)
        print(f"success publish score, status Code: {r.status_code}")
    except:
        print("error publish score, might ne network error")


# exit game
def quit_game():
    pygame.mixer.music.stop()
    pygame.mixer.quit()
    pygame.quit()
    quit()


# game loop
while True:
    if home:
        # set bg
        #screen.fill((0, 0, 0))
        screen.blit(str_img,background_rect)

        # reigster events
        clock.tick(15)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == pygame.K_RETURN:
                    name_game()
                if event.key == K_ESCAPE:
                    pygame.quit()
                    quit()
            if event.type == QUIT:
                pygame.quit()
                quit()

    elif name:
        # set bg
        #screen.fill((0, 0, 0))
        screen.blit(name_img,background_rect)

        # reigster events
        clock.tick(15)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    quit()
            if event.type == QUIT:
                pygame.quit()
                quit()
            name_box.handle_event(event)

        name_box.draw(screen)

    elif running:
        # set bg
        #screen.fill((0, 0, 0))
        screen.blit(run_img,background_rect)

        # set score
        text = small_text.render("Score: " + str(score), True, black)
        screen.blit(text, ((SCREEN_WIDTH - 120), 10))

        # register event
        clock.tick(45)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    home = True
                    running = False

            elif event.type == QUIT:
                home = True
                running = False

            elif event.type == ADDENEMY:
                new_enemy = Enemy()
                enemies.add(new_enemy)
                all_sprites.add(new_enemy)

            elif event.type == ADDCLOUD:
                new_cloud = Cloud()
                clouds.add(new_cloud)
                all_sprites.add(new_cloud)

        pressed_keys = pygame.key.get_pressed()
        player.update(pressed_keys)
        enemies.update()
        clouds.update()
        bullets.update()

        # collide cloud
        hits = pygame.sprite.spritecollide(player, clouds, True)
        if len(hits) > 0:
            # handle sounds
            move_down_sound.stop()
            collision_sound.play()

            # update score
            score +=  10

            # refresh speed
            dt = time.time() - last_time
            if dt > 8:
                last_time = time.time()
                print("enemy speed " + str(enemy_speed))
                if enemy_speed > 200:
                    enemy_speed -= 200
                    pygame.time.set_timer(ADDENEMY, enemy_speed)
                elif enemy_speed <= 200 and enemy_speed > 50:
                    enemy_speed -= 50
                    pygame.time.set_timer(ADDENEMY, enemy_speed)

        # collide enemi
        elif pygame.sprite.spritecollideany(player, enemies):
            move_up_sound.stop()
            move_down_sound.stop()
            collision_sound.play()

            # kill and restart
            player.kill()
            end_game()

        # collied bulets
        hits = pygame.sprite.groupcollide(enemies, bullets, True, True)
        for hit in hits:
            explore_sound.play()

        all_sprites.draw(screen)

    # game end
    elif end:
        # set bg
        #screen.fill((0, 0, 0))
        screen.blit(end_img,background_rect)

        # reigster events
        clock.tick(15)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == pygame.K_RETURN:
                    start_game()
                if event.key == K_ESCAPE:
                    pygame.quit()
                    quit()
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        text = large_text.render(str(score), True, black)
        text_rect = text.get_rect(center=(SCREEN_WIDTH/2, (SCREEN_HEIGHT/2) + 40))
        screen.blit(text, text_rect)


    pygame.display.flip()


quit_game()
